#ifndef SIMGUI_LOGGER_H
#define SIMGUI_LOGGER_H

#include <fstream>

#define DEFAULT_FILE "info.log"

enum Verbosity {
    // Just information
            INFO,
    // Something bad, but not critical
            WARN,
    // Error
            ERR,
};

class Logger {
public:
    static void log(std::string);
    static void log(std::string, Verbosity);
private:
    Logger();
    ~Logger();
    std::ofstream *file;
    void timestamp() const;
    static const Logger i;
};

#endif //SIMGUI_LOGGER_H
