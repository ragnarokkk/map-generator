#include <ctime>

#include "Logger.h"

std::string toString(Verbosity &v) {
    switch (v) {
        case INFO:
            return "[INFO] ";
        case WARN:
            return "[WARN] ";
        case ERR:
            return "[ERR] ";
    }
    return " ";
}

const Logger Logger::i;

void Logger::log(std::string s) {
    log(s, INFO);
}

void Logger::log(std::string s, Verbosity v) {
    i.timestamp();
    *i.file<<toString(v)<<s<<'\n';
    i.file->flush();
}

Logger::Logger() {
    file = new std::ofstream(DEFAULT_FILE, std::ios::app);
    timestamp();
    *file<<"Logging started!\n\n";
}

Logger::~Logger() {
    file->close();
    delete file;
}

void Logger::timestamp() const {
    std::ofstream &f = *file;
    time_t t = time(0);
    struct tm * now = localtime( & t );
    f << "###\t"
      << (now->tm_year + 1900) << '-'
      << (now->tm_mon + 1)     << '-'
      <<  now->tm_mday         << ' '
      <<  now->tm_hour         << ':'
      <<  now->tm_min          << ':'
      <<  now->tm_sec          << '\n';
}
